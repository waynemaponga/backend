const express = require('express')
const app = express()
const mongoose = require('mongoose');
const User = require('./models/User');
mongoose.connect("mongodb+srv://wayne:wayne@cluster0-taj1a.mongodb.net/test?retryWrites=true&w=majority");
mongoose.Promise = global.Promise;
var jwt = require('jsonwebtoken');
var config = require('./config');



//Listen on port 3000
server = app.listen(4000)



//socket.io instantiation
const io = require("socket.io")(server)


//listen on every connection
io.on('connection', (socket) => {
	console.log('New user connected')

	//default username
	//socket.username = "Anonymous"

    //listen on change_username
    socket.on('registerUser', (userDTO) => {
        var usermodel = new User({
            firstname:userDTO.firstname,
            lastname: userDTO.lastname,
            email: userDTO.email,
            password: userDTO.password,
            mobileNumber: userDTO.mobileNumber,
          });
          
          usermodel.save(function(err) {
              if(err){
                if(err.errmsg.search('email_1 dup key')) {
           
                    socket.emit('message',{ code: ' 422',description: 'An account for the specified email address already exists. Try another email address'});
                }
              }
           else{
                socket.emit('message',{ code: '201', description: 'Succesfully Registered'});
            }
    
            
          });



    });
 
    
    socket.on('login', (userDTO) => {
        var usermodel = new User({
            firstname:userDTO.firstname,
            lastname: userDTO.lastname,
            email: userDTO.email,
            password: userDTO.password,
            mobileNumber: userDTO.mobileNumber,
          })
                 
          User.findOne({
            email:  userDTO.email
        }, function(err, user) {
    
    
            if(!user){

                socket.emit('message',{ code: '401', description: 'Incorrect Email or Password'}); 
            }else {
    
                user.comparePassword(userDTO.password, function(err, isMatch) {
                    if (err) throw err;
       
    
    
                    if(isMatch==true){
                        var token = jwt.sign({ user: user }, config.secret, {
                            expiresIn: 86400 // expires in 24 hours
                        });
           
                        socket.emit('message',{ code: '200' ,auth: true, token: token }); 
                    }else{
                       
                        socket.emit('message',{ code: '401', description: 'Incorrect Email or Password'}); 
                    }
                });
    
    
            }
    
    
        });
          
        
 
});
 
})
